import json
import jwt

from sanic import Sanic
from sanic.response import json as json_res
from sanic_jwt import exceptions

app = Sanic("Ployrize")


@app.route("/auth",  methods=["POST", ])
async def authenticate(request):
    username = request.json.get("username", None)
    password = request.json.get("password", None)

    if not username or not password:
        raise exceptions.AuthenticationFailed("Missing username or password.")

    with open('users.txt') as json_file:
        data = json.load(json_file)
        if username not in data:
            raise exceptions.AuthenticationFailed("User not found.")

        if data[username] != password:
            raise exceptions.AuthenticationFailed("Password is incorrect.")

    encoded_jwt = jwt.encode({'username': username, 'password': data[username]}, 'secret', algorithm='HS256')

    # TODO store token


@app.route("/normalize",  methods=["POST", ])
async def normalize(req):
    # TODO Check JWT token in header, preferably in a blueprint - jwt.decode == one of our tokens

    return json_res(
        {item['name']: item[next((key for key in item.keys() if 'val' in key.lower()))] for item in req.json}
    )

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)
