class MagicList:
    def __init__(self, cls_type=None):
        self._list = []
        self._cls_type = cls_type

    def __getitem__(self, index: int) -> object:
        if not isinstance(index, int) or len(self._list) < index:
            raise IndexError('list index out of range')
        elif len(self._list) == index:
            self._list.append(self._cls_type() if self._cls_type else None)

        return self._list[index]

    def __setitem__(self, index: int, value: object):
        if not isinstance(index, int) or len(self._list) < index:
            raise IndexError('list index out of range')

        self._list.append(self._cls_type() if self._cls_type else None)

        self._list[index] = value

    def __str__(self) -> str:
        return str(self._list)
